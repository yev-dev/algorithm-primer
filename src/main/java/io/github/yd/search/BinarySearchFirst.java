package io.github.yd.search;

import java.util.Arrays;

public class BinarySearchFirst {

	public static void main(String[] args) {
		
		int key = 4; // index of this key we are after
		
		int[] array = {10,9,8,7,6,5,4,3,2,1};
//		{1,2,3,4,5,6,7,8,9,10}
		
		Arrays.sort(array);
		
		
		System.out.println(binarySearch_1(4,array));
		System.out.println(binarySearch_1(89,array));
		
		
	}
	
	public static int binarySearch_1(int key, int[] array) {
		
		int lowerBound = 0;
		int upperBound = array.length - 1;
		
		if(array[lowerBound] > array[upperBound]) {
			return -1;
		}
		else if(key > array[upperBound]){
			return -1;
		}
		
		while(lowerBound < upperBound) {
			
			int midPoint = (lowerBound + upperBound ) / 2;
			
			//check of the key is the id value to return
			if(key == array[midPoint]) {
				return midPoint;
			}
			
			//switching mid point to become either lower or upper bound based on the value it holds
			//As we have evaluated the id point, then we have to increment/decrement in appropriate direction
			if(key > array[midPoint]) {
				lowerBound = midPoint + 1;
			}else {
				upperBound = midPoint - 1;
			}
			
		}
		return lowerBound;
	}

}
