package io.github.yd.sort;

import java.util.Arrays;

/*
 * Inefficient sort using sub-array to store sorted values
 */
public class InsertionSort {

	public static void main(String... args) {
		int[] array = { 10, 2, 3, 4, 5, 6, 7, 8, 9, 1 };
		insertionSort(array);
		System.out.println(Arrays.toString(array));
		
	}

	public static void insertionSort(int[] array) {

		for (int i = 1; i < array.length; i++) {
			int val = array[i];
			for (int j = i - 1; j >= 0 && array[j] > val; j--) {
				int tmp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = tmp;
//				System.out.println(tmp);
//				System.out.println(array[j]);
//				System.out.println(array[j +1]);
				System.out.println(array[i]);
			}
			System.out.println(array);
		}
	}

}
