package io.github.yd.miscellaneous;

/**
 * Given a sorted array and a target value, return the index if the target is found. 
 * If not, return the index where it would be if it were inserted in order
 * Basically, do the same what Collections.binarySearch does
 * Array is sorted
 *	[1,3,5,6], 5 -> 2
 *	[1,3,5,6], 0 -> 0
 */
public class InsertionPoint {

	public static void main(String[] args) {
		
		int[] values = {1,3,5,6};
		
		System.out.println(binarySearch(values,5)); //2
		System.out.println(binarySearch(values,2)); //1

	}

	// complexity => O(n) by iterating all elements
	static int naiveSearchInsert(int[] values, int target) {
		if(values == null) return 0;
		if(target <= values[0]) return 0;
		int i = 0;
		while(i < values.length -1) {
			if( target > values[i] && target <= values[i+1]) {
				return i+1;
			}
			i++;
		}
		return 0;
	}
	
	// O(ln(n))
	static int binarySearch(int[] values, int target) {
		
		int i = 0;
		int j = values.length - 1;
		
		while(i <= j) {
			int mid = (i + j) / 2;
			if(target > values[mid])
				i = mid + 1;
			else if(target < values[mid])
				j = mid -1;
			else
				return mid;
		}
		
		return i;
	}
}
