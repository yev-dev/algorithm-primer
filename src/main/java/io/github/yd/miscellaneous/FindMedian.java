package io.github.yd.miscellaneous;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Given a list of unsorted elements, find the median.
 * Optimize an algorithm not to run 
 * @author yyermosh
 *
 */
public class FindMedian {

	public static void main(String[] args) {
		List<Double> evenValues = new ArrayList<>(Arrays.asList(2.0,1.0,5.0,4.0));
		List<Double> oddValues = new ArrayList<>(Arrays.asList(2.0,1.0,5.0,4.0, 2.0));
		
		Collections.sort(evenValues);
		Collections.sort(oddValues);
		
		System.out.println("Even values after sorting " + evenValues);
		System.out.println("Odd values after sorting " + oddValues);
		System.out.println("Median for evens :[" + getMedian(evenValues) + "]");
		
		System.out.println("Median for odds :[" + getMedian(oddValues) + "]");
		
		
		double newValue = 5.0;
		
		System.out.println("Adding a new value " + newValue + " to " + oddValues);

		oddValues.add(newValue);
		//Determine an insertion point to avoid re-sorting
		int insertionPoint = Collections.binarySearch(oddValues, newValue);
		
		System.out.println("Insertion point " + insertionPoint);
		
		if(insertionPoint > 0) { //already exists. 
			oddValues.add(++insertionPoint, newValue);
		}
		else {//a new element
			oddValues.add(Math.abs(insertionPoint + 1), newValue);
		}
		
		System.out.println("New list " + oddValues);
	}
	
	private static double getMedian(List<Double> values) {
		
		int size = values.size();
		int midPoint = size / 2;
		
		/**
		 * If there is an odd number of results, the median is the middle number
		 * If there is an even number of results, the median will be the mean of the two central numbers.
		 */
		if( size % 2 > 0) {//odd
			return values.get(midPoint);
		}
		else {
			return (values.get(midPoint-1) + values.get(midPoint)) / 2;
		}
		
	}


}
