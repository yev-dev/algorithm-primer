package io.github.yd.miscellaneous;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Assert;
import org.junit.Test;

public class BatchingTest {

	@Test
	public void testBatchingWithSublist() {

		List<Integer> values = IntStream.range(0, 105).boxed().collect(Collectors.toList());
		// System.out.println(values);
		List<List<Integer>> batches = new ArrayList<>();

		int batchSize = 5;

		for (int i = 0; i < values.size(); i = i + batchSize) {
			batches.add(values.subList(i, Math.min(values.size(), i + batchSize)));
		}
		Assert.assertEquals(batches.size(), 21);
	}
	

}
