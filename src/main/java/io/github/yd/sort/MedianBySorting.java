package io.github.yd.sort;

import java.util.Arrays;

public class MedianBySorting {

	public static void main(String[] args) {

		int[] array1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		System.out.println(medianWithSort(array1));
	}

	static int medianWithSort(int[] array) {
		Arrays.sort(array);
		int index = (array.length - 1) / 2;
		
		//TODO: add even/odd
		return array[index];
	}

	static int partition(int[] array, int pos, int offset, int length) {
		if (length <= 1) {
			return offset;
		}
		int val = array[pos];
		array[pos] = array[offset];
		array[offset] = val;
		int left = offset;
		int right = offset + length - 1;
		while (left < right) {
			if (val < array[right]) {
				right--;
			} else {
				int tmp = array[left + 1];
				array[left + 1] = array[left];
				array[left] = tmp;
				if (right != (left + 1)) {
					array[left] = array[right];
					array[right] = tmp;
				}
				left++;
			}
		}
		return left;
	}

}
