package io.github.yd.sort;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

/**
 * O(n^2)
 *
 */
public class BubbleSort {

	public static int[] bubbleSort1(int[] values) {

		boolean numberSwitched;

		do {
			numberSwitched = false;

			for (int i = 0; i < values.length - 1; i++) {
				if (values[i + 1] < values[i]) {
					int tmp = values[i + 1];
					values[i + 1] = values[i];
					values[i] = tmp;
					numberSwitched = true;
					System.out.println(i);
				}
				System.out.println("outside");
			}

		} while (numberSwitched);

		return values;
	}

	public static int[] bubbleSort2(int[] values) {
		int j = values.length - 1;
		for (int i = 0; i <= j; i++) {
			if (values[i + 1] < values[i]) {
				int tmp = values[i + 1];
				values[i + 1] = values[i];
				values[i] = tmp;
				System.out.println(i);
			}
			
		}
		return values;
	}

	@Test
	public void testBubbleSort() {
		int[] shouldBe = { 1, 2, 3, 4, 5 };

		int[] numbers = bubbleSort1(new int[] { 5, 4, 2, 3, 1 });

		assertTrue(Arrays.equals(bubbleSort1(numbers), shouldBe));
	}
}
