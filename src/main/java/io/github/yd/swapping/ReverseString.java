package io.github.yd.swapping;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Stack;
import java.util.StringTokenizer;

import org.junit.Test;

public class ReverseString {


	@Test
	public void reverseStringByChar() {

		String s = "ABCD";
		String reversed = "DCBA";

		char[] characters = s.toCharArray();

		StringBuilder sb = new StringBuilder();

		//Naive approach

		for(int i = characters.length - 1; i >= 0; i--) {
			sb.append(characters[i]);
		}

		System.out.println(sb.toString());

		assertTrue("Should be equal", reversed.equals(sb.toString()));

		char[] copyOfCharacter = Arrays.copyOf(characters, characters.length);


		for(int i = 0, y = copyOfCharacter.length -1; i < copyOfCharacter.length / 2; i++, y--) {
			char tmp = copyOfCharacter[i];
			copyOfCharacter[i] = copyOfCharacter[y];
			copyOfCharacter[y] = tmp;
		}

		System.out.println(String.valueOf(copyOfCharacter));

		assertTrue("Should be equal", reversed.equals(String.valueOf(copyOfCharacter)));
	}

	@Test
	public void reversStringByWordWithStack() {

		String str1 = "reverse this string";

		String reversedStr = "string this reverse";

		Stack<String> stack = new Stack<>();

		StringTokenizer strTok = new StringTokenizer(str1);

		while(strTok.hasMoreTokens()) {
			stack.push(strTok.nextToken());
		}

		StringBuilder sb = new StringBuilder();

		while(!stack.isEmpty()) {
			sb.append(stack.pop());
			sb.append(" ");
		}

		assertTrue("Should be equal", sb.toString().trim().equals(reversedStr));

	}
}
