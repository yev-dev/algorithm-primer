package io.github.yd.swapping;


import java.util.Arrays;

import io.github.yd.utils.Utils;
import org.junit.Test;


/**
 * Swap array's elements [1,2,3] => [3,2,1]
 * @author yev
 *
 */
public class ReverseArraySwapping {

	static int[] reverseSwapping1(int[] values) {
		for(int i = 0, y = values.length - 1; i < values.length / 2; i++, y--) {
			int temp = values[i];
			values[i] = values[y];
			values[y] = temp;
		}
		Utils.print(values);
		return values;
	}
	static int[] reverseSwapping2(int[] values) {
		int i = 0;
		int y = values.length -1;
		
		while(i < values.length / 2) {
			int temp = values[i];
			values[i] = values[y];
			values[y] = temp;
			i++;
			y--;
		}
		Utils.print(values);
		
		return values;
	}
	
	@Test
	public void testSwapping() {
		int[] values = {1,2,3};
		reverseSwapping1(Arrays.copyOf(values, values.length));
		reverseSwapping2(Arrays.copyOf(values, values.length));
	}

}
