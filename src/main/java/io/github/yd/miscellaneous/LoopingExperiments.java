package io.github.yd.miscellaneous;

public class LoopingExperiments {

	
	public static void main(String... args) {
		int[] a = {7,8,4,4,6,7,10};
		
		for (int i = 1; i < a.length; i++) {
			int value = a[i];
			System.out.println("value :" + value);
			for(int j = i-1 ; j >= 0 && a[j] > value; j-- ) {
				System.out.println("In the inner loop");
				int tmp = a[j];
				System.out.println("a[j] :" + a[j]);
				System.out.println("a[j + 1] :" + a[j + 1]);
				System.out.println("tmp :" + tmp);
				
				a[j] = a[j + 1];
				a[j + 1] = tmp;
                
			}
		}
	}
}
