package io.github.yd.utils;

import java.util.Random;

public class TestHarness {

	public static final int SIZE = 100;
	public static int[] values = new int[SIZE];

	public static void initValues() {
		Random rand = new Random();
		for (int index = 0; index < SIZE; index++)
			values[index] = Math.abs(rand.nextInt()) % 100;
	}

	public static boolean isSorted() {
		if (values[0] > values[SIZE - 1])
			return false;

		for (int i = 0; i < SIZE - 1; i++) {
			if (values[i] > values[++i])
				return false;
		}
		return true;
	}

	public static void swap(int index1, int index2) {
		if (index1 >= SIZE || index2 >= SIZE)
			return;

		int temp = values[index1];
		values[index1] = values[index2];
		values[index2] = values[temp];

	}

	public static int randInt(int min, int max) {
		Random rand = new Random();
		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomNum = rand.nextInt((max - min) + 1) + min;

		return randomNum;
	}
}
