package io.github.yd.sort;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ComparableAndComparatorSorting {

	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Test
	public void testPersonComparableSort() {
//		exception.expect(ClassCastException.class);
		final List<NotComparable> objects = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
	        objects.add(new NotComparable(i));
	    }
		
		Arrays.sort(objects.toArray());
		
	}
	
	@Test
	public void testComparablePerson() {
		
		List<Person> people = new ArrayList<>(Arrays.asList(new Person("Yev","5 Lynwood",38), new Person("Dina","5 Lynwood",34)));
		
		System.out.println(people);

		Collections.sort(people);
		
		
	}
	
	private static class NotComparable {
	    private int i;
	    private NotComparable(final int i) {
	        this.i = i;
	    }
	}
	
	@Test
	public void customSorting() {
	    final List<Integer> numbers = Arrays.asList(4, 7, 1, 6, 3, 5, 4);
	    final List<Integer> expected = Arrays.asList(7, 6, 5, 4, 4, 3, 1);

	    Collections.sort(numbers, new ReverseNumericalOrder());
	    assertEquals(expected, numbers);
	}
	
	public class ReverseNumericalOrder implements Comparator<Integer> {
	    @Override
	    public int compare(Integer o1, Integer o2) {
	        return o2 - o1;
	    }
	}
}
