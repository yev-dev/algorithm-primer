package io.github.yd.sort;

public class Person implements Comparable<Person> {

	private String name;
	private String address;
	private int age;
	
	
	public Person(String name, String address, int age) {
		super();
		this.name = name;
		this.address = address;
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Person [name=" + name + ", address=" + address + ", age=" + age + "]";
	}
	//sort by age, name  desc
	@Override
	public int compareTo(Person o) {
		int ageCmp = this.age - o.getAge();
		
		if(ageCmp != 0)
			return ageCmp;
		
		return this.name.compareTo(o.getName());
	}
	
	
}
