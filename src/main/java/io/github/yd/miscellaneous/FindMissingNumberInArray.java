package io.github.yd.miscellaneous;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

/**
 * If ‘n’ is the positive number and ‘a’ is an array of integers of length n-1
 * containing elements from 1 to n. Then find the missing number in ‘a’ in the
 * range from 1 to n. Occurrence of each element is only once. i.e ‘a’ does not
 * contain duplicates.
 */
public class FindMissingNumberInArray {

	static int naiveFind(int[] array){

		Arrays.sort(array);

		for(int i=0; i < array.length; i++){
			if(array[i+1] != i+1)
				return array[i] + 1;
		}

		return 0;
	}

	static int missingNumberWithoutSorting(int[] array){
		//get a sum of all values

		int sum = 0;
		for(int i = 0; i < array.length; i++) {
			sum += array[i];
		}

		// calculate sum of natural numbers
		int n = 8; //expected number of elements
		int naturalSum = (n * (n + 1)) / 2;

		return naturalSum - sum;

	}

	@Test
	public void testArray(){
		int[] array = { 2, 4, 5, 3, 7, 8, 6 };
		assertEquals("Naive search to find a missing number",2, naiveFind(array));
		assertEquals("Search to find without sorting",2, missingNumberWithoutSorting(array));

	}

}
