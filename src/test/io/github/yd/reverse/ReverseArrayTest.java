package io.github.yd.reverse;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class ReverseArrayTest {

	@Test
	public void testReverseArray() {
		int[] array = {1,2,3,4,5,6,7,8,9,10};
		int[] expectedReversedArray = {10,9,8,7,6,5,4,3,2,1};
		
		int[] reversedArray1 = ReverseArray.reverseArray_1(array);
		System.out.println(Arrays.toString(reversedArray1));
		int[] reversedArray2 = ReverseArray.reverseArray_2(array);
		System.out.println(Arrays.toString(reversedArray2));
		
		Assert.assertTrue(Arrays.equals(reversedArray1, expectedReversedArray));
		Assert.assertTrue(Arrays.equals(reversedArray2, expectedReversedArray));
	}
}
