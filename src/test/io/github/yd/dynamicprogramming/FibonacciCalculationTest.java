package io.github.yd.dynamicprogramming;

import org.junit.Test;

public class FibonacciCalculationTest {

	@Test
	public void testFibonacciCalculationWithoutRecursion() {
		
		
//		long startTime_1 = System.currentTimeMillis();
//		int result_1 = FibonacciCalculation.fibonacciRecursive(5);
//		System.out.println(result_1);
//		long endTime_1 = System.currentTimeMillis();
//		System.out.println(endTime_1 - startTime_1);
		
		long startTime_2 = System.currentTimeMillis();
		int result_2 = FibonacciCalculation.fibonacciWithCache(5);
		System.out.println(result_2);
		long endTime_2 = System.currentTimeMillis();
		System.out.println(endTime_2 - startTime_2);

		long startTime_3 = System.currentTimeMillis();
		int result_3 = FibonacciCalculation.fibonacciDP(5);
		System.out.println(result_3);
		long endTime_3 = System.currentTimeMillis();
		System.out.println(endTime_3 - startTime_3);
	}
}
