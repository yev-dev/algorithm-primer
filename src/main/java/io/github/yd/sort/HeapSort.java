package io.github.yd.sort;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;

/*
 * Uses a priority queue to achieve optimal worst case behaviour
 * Since building the heap and removing the max requires no external memory, heap sort is an in-place algorithm
 */
public class HeapSort {

	public static void main(String[] vars) {
		int[] array = {10,2,3,4,5,6,7,8,9,1};
		heapSort(array);
		System.out.println(Arrays.toString(array));
		
		reversedHeapSort(array);
		System.out.println(Arrays.toString(array));
	}
	
	public static void heapSort(int[] array) {
		PriorityQueue<Integer> heap = new PriorityQueue<>();

		for (int value : array) {
			heap.add(value);
		}
		
		for(int i = 0; i < array.length; i++) {
			array[i] = heap.poll();
		}
	}
	
	public static void reversedHeapSort(int[] array) {
		
		Comparator<Integer> reversedComparator = (x, y) -> y.compareTo(x);
		
		PriorityQueue<Integer> heap = new PriorityQueue<>(reversedComparator);
		
		Iterator<Integer> iter = heap.iterator();
		
		
		for (int value : array) {
			heap.add(value);
		}
		
		for(int i = 0; i < array.length; i++) {
			array[i] = heap.poll();
		}
	}
	
	
}
