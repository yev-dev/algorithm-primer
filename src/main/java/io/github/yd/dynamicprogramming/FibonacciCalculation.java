package io.github.yd.dynamicprogramming;

import java.util.HashMap;
import java.util.Map;

public class FibonacciCalculation {

	private static Map<Integer, Integer> cache = new HashMap<>();

	public static int fibonacciRecursive(int n) {
	
		if(n < 3) {
			return 1;
		}
		
		return fibonacciRecursive(n -2) + fibonacciRecursive(n -1);
	}
	
	// F(n) in 0(n) time and 0(1) space
	// Iteratively fills in the cache in a bottom-up fashin to reduce the space
	// complexity
	public static int fibonacciDP(int n) {
		if(n < 3) {
			return 1;
		}

		int fMinus2 = 0;
		int fMinus1 = 1;

		for (int i = 2; i <= n; ++i) {

			int f = fMinus2 + fMinus1;
			fMinus2 = fMinus1;
			fMinus1 = f;
		}
		return fMinus1;

	}

	public static int fibonacciWithCache(int n) {
		if(n < 3) {
			return 1;
		}

		if (!cache.containsKey(n)) {
			cache.put(n, fibonacciWithCache(n - 2) + fibonacciWithCache(n - 1));
		}

		return cache.get(n);
	}
}
