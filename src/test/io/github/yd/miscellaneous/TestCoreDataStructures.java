package io.github.yd.miscellaneous;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class TestCoreDataStructures {

	@Test
	public void testLRU() {
		List<String> l = new LinkedList<>();
		
	}
	
	@Test
	public void testLooping(){
		
		int[] array = {1,2,3,4,5};

		int left = 0;
		int right = array.length -1;
		
		int pivot = left + ((right - left)/2);
		
		
		while(left < right) {
			int temp = array[left];
			array[left] = array[right];
			array[right] = temp;
			
			left++;
			right--;
		}
	}
	@Test
	public void testHashCode() {
		Map<Person,String> numbers = new HashMap<>();
		
		
		
		Person p1 = new Person();
		p1.name = "A";
		
		Person p2 = new Person();
		p2.name = "B";
		
		numbers.put(p1, "1");
		numbers.put(p2, "2");

		System.out.println(numbers);
		
		System.out.println(numbers.get(p1));
	}
	
	private class Person{
		
		public String name;

		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

//
//		@Override
//		public boolean equals(Object obj) {
//			if (this == obj)
//				return true;
//			if (obj == null)
//				return false;
//			if (getClass() != obj.getClass())
//				return false;
//			Person other = (Person) obj;
//			if (!getOuterType().equals(other.getOuterType()))
//				return false;
//			if (name == null) {
//				if (other.name != null)
//					return false;
//			} else if (!name.equals(other.name))
//				return false;
//			return true;
//		}


		@Override
		public String toString() {
			return "Person [name=" + name + "]";
		}


		private TestCoreDataStructures getOuterType() {
			return TestCoreDataStructures.this;
		}

		
		
	}
}
