package io.github.yd.cache;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/*
 * This particular example uses the hash table to store the data
 * Linked-list is used to maintain priority. The head of the linked-list always contains the most recently accessed item,
 * and the tail the least recently accessed item.
 * We have a constant time for all operations
 */
public class MRUCache {

	private final LinkedList<String> list;
	private final Map<String,Integer> table;
	private int boundary;
	
	public MRUCache(int boundary) {
		super();
		this.boundary = boundary;
		this.list = new LinkedList<>();
		this.table = new HashMap<>();
	}
	
	
	public Integer find(String key) {
		
		Integer value = table.get(key);
		
		if(value != null) {
			list.remove(key);
			list.offerFirst(key);
			return value;
		}
		return null;
	}
	
	public void add(String key, Integer item) {
		
		list.push(key);
		table.put(key, item);
		
		if(table.size() > boundary) {
			table.remove(list.getLast());
			list.removeLast();
		}
		
		
		
	}
	
	
}
