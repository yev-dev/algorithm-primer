package io.github.yd.reverse;

import java.util.Arrays;

public class ReverseArray {

	public static int[] reverseArray_1(int[] array) {
		int[] copyOfArray =  Arrays.copyOf(array, array.length);
		int left = 0;
		int right = copyOfArray.length - 1;
		int pivot = left + ((right - left) / 2);
		
		for(int i = left, j = right; i <= pivot; i++, j-- ) {
			int tmp = copyOfArray[i];
			copyOfArray[i] = copyOfArray[j];
			copyOfArray[j] = tmp;
		}
		return copyOfArray;
		
	}
	public static int[] reverseArray_2(int[] array) {
		int[] copyOfArray =  Arrays.copyOf(array, array.length);
		int left = 0;
		int right = copyOfArray.length - 1;
		
		while(left < right) {
			int tmp = copyOfArray[left];
			copyOfArray[left] = copyOfArray[right];
			copyOfArray[right] = tmp;
			left++;
			right--;
		}
		return copyOfArray;
		
	}
}
